<div class="featured two-third">
    <header class="s-title">
        <h2 class="ribbon">Special Recipe of the Day</h2>
    </header>
    <?php
    $sql =$obj->FlyQuery("SELECT recipes.*,COUNT(recipes_view.id) FROM `recipes` 
LEFT JOIN recipes_view ON recipes.id=recipes_view.recipes_id
GROUP BY recipes.id order by COUNT(recipes_view.id) DESC LIMIT 1");
    foreach ($sql as $chef) {
      
    
    ?>
    

    <article class="entry">
        <figure>
            <img src="<?= SITE_IMG_PATH ?><?php echo $chef->image; ?>" style="height: 560px!important; width:100%;" alt="special_recipes_day_image" />
            <figcaption><a href="<?= APP_PATH ?>recipe_view.php?rec_id=<?=$chef->id?>"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
        </figure>
        <div class="container">
            <h2><a href="<?= APP_PATH ?>recipe_view.php?rec_id=<?=$chef->id?>"><?php echo $chef->name;?></a></h2>
            <p><?php echo $chef->directions;?> </p>
            <div class="actions">
                <div>
                    <a href="<?= APP_PATH ?>recipe_view.php?rec_id=<?=$chef->id?>" class="button">See the full recipe</a>
<!--                    <div class="more"><a href="recipes2.html">See past recipes of the day</a></div>-->
                </div>
            </div>
        </div>
    </article>
    <?php }?>
    
</div>
<!--    //SELECT * FROM `recipes` WHERE `id`=(SELECT `recipes_id` FROM recipes_view  GROUP BY recipes_id ORDER BY COUNT(id) DESC LIMIT 1)
//
/*  
 * 
SELECT recipes.*,COUNT(recipes_view.id) FROM `recipes` 
LEFT JOIN recipes_view ON recipes.id=recipes_view.recipes_id
GROUP BY recipes.id order by COUNT(recipes_view.id) DESC LIMIT 1
 * 
 */
//-->