<?php

session_start();
//bulid query
include_once '../lib/settings.php';
include_once '../lib/connection.php';
include_once  '../lib/auth.php'; 

extract($_POST);
if (!empty($recipes_category) && !empty($recipes_category_date)) {
//    $query = "INSERT INTO  recipes_category  SET name='$recipes_category',is_top_nav='$category',date='" . date('Y-m-d', strtotime($recipes_category_date)) . "',status='$category_status'";
//
//
//    $result = $db->exec($query);
//var_dump($result);
    $category=$category?1:0;
    $category_status=$category_status?1:0;
     $insert_array = array('name' => $recipes_category, 'is_top_nav' => $category,'date' =>date('Y-m-d', strtotime($recipes_category_date)), 'status' => $category_status);
    $result = $obj->Insert("recipes_category", $insert_array);

    if ($result) {
        session_regenerate_id();
        $_SESSION['SESS_MSG_SUCCESS'] = 'Information Successfully Saved';
        session_write_close();
        header('location: ' . APP_PATH . 'recipes_category/list_recipes_category.php');
        exit();
    } else {
        session_regenerate_id();
        $_SESSION['SESS_MSG_ERROR'] = 'Failed to save, Please try again.';
        session_write_close();
        header('location: ' . APP_PATH . 'recipes_category/add_recipes_category.php');
        exit();
    }
    header("location:list_recipes_category.php");
} else {
    session_regenerate_id();
    $_SESSION['SESS_MSG_ERROR'] = 'Please Fillup Mandatory Field';
    session_write_close();
    header('location: ' . APP_PATH . 'recipes_category/add_recipes_category.php');
    exit();
}