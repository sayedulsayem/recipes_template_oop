<?php
session_start();
//bulid query
include_once '../lib/settings.php';
include_once '../lib/connection.php';
include_once '../lib/auth.php';
extract($_POST);

// $query = "UPDATE  recipes_category  SET name='$recipes_category',is_top_nav='$category',date='$recipes_category_date',status='$category_status'";
//
//
//$result = $db->exec($query);
//var_dump($result);
$category_status=$category_status?1:0;
$category=$category?1:0;

 $update_array=array('name'=>$recipes_category,'is_top_nav'=>$category,'date'=>date('Y-m-d',strtotime($recipes_category_date)),'status'=>$category_status);
 $update_condition_array=array('id'=>$id);
 $result = $obj->Update("recipes_category",$update_array,$update_condition_array);

if($result){
      session_regenerate_id();
    $_SESSION['SESS_MSG_SUCCESS'] = 'Deleted Successfully';
    session_write_close();
    header('location:' . APP_PATH . 'recipes_category/list_recipes_category.php');
    exit();
}else{
    session_regenerate_id();
    $_SESSION['SESS_MSG_ERROR'] = 'Failed to Deleted.';
    session_write_close();
    header('location:' . APP_PATH . 'recipes_category/list_recipes_category.php');
    exit();
}