<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
include_once  '../lib/auth.php'; 
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title> Customers Recipes Request | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Customers Recipes Request
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center" >
                                <strong> <h4>Update Customers recipes request List</h4></strong>
                            </div>
                              <?php
                            //build query

                            $queryup = $obj->FlyQuery("SELECT * FROM customer_recipes_request WHERE id = " . $_GET['id']);
                            //excute the query useing php
                            foreach ($queryup as $editDatapart) {
                              
                            }
                            ?>
                            <?php include '../element/msg.php'; ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>customer_recipes_request/update_customer_recipes_request.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editDatapart->id; ?>">




                                            <div class="form-group">
                                                <label for="customer_request">Customers Recipes Request Status </label>

                                                <select class="form-control"  name="customer__recipes_request_status" id="contact_request" required="required" alt="customer_request">
                                                    <option>Select Customer Request Status</option>
                                                    <option value="Approve">Approve</option>
                                                    <option value="Cancel">Cancel</option>
                                                    <option value="Pending">Pending</option>
                                                   

                                                </select>


                                            </div>


                                            <button type="submit" class="btn btn-default"> Update Customer Recipes Request status</button>
                                            <!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->



























