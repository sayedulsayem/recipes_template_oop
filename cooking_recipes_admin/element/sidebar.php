<!--/* Author : Munira *-->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="background-color: #d43f3a;">

            <div class="pull-left info">
                <p> Logged Admin : <?=$_SESSION['SESS_ADMIN_NAME']?></p>

<!--                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
<!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
<!--                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>-->
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li>
                <a href="<?= APP_PATH ?>index.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span>Admin</span> 

                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li> <a href="<?= APP_PATH ?>admin/add_new_admin.php"><i class="fa fa-angle-double-right"></i>Add New Admin </a>  </li>


                    <li><a href="<?= APP_PATH ?>admin/list_admin.php"><i class="fa fa-angle-double-right"></i>Admin List </a> </li>



                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-spoon" aria-hidden="true"></i>
                    <span>Recipes</span> 

                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li> <a href="<?= APP_PATH ?>recipes/recipes.php"><i class="fa fa-angle-double-right"></i>Add Recipes </a>  </li>


                    <li><a href="<?= APP_PATH ?>recipes/list_recipes.php"><i class="fa fa-angle-double-right"></i>Recipes List </a> </li>



                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>

                    <span>Recipes category</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>recipes_category/add_recipes_category.php"><i class="fa fa-angle-double-right"></i>Add Recipes Category</a></li>
                    <li><a href="<?= APP_PATH ?>recipes_category/list_recipes_category.php"><i class="fa fa-angle-double-right"></i> Recipes Category List</a></li>


                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-star-half-o" aria-hidden="true"></i>

                    <span>Recipes rating</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>recipes_rating/list_recipes_rating.php"><i class="fa fa-angle-double-right"></i>Recipes Rating List</a></li>


                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-video-camera" aria-hidden="true"></i>
                    <span>Recipes video</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>recipes_video/add_recipes_video.php"><i class="fa fa-angle-double-right"></i>Add Recipes Video link</a></li>
                    <li><a href="<?= APP_PATH ?>recipes_video/list_recipes_video_link.php"><i class="fa fa-angle-double-right"></i> Recipes Video link List</a></li>

                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    <span>Recipes view</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>recipes_view/list_recipes_view.php"><i class="fa fa-angle-double-right"></i>Recipes View List</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cutlery" aria-hidden="true"></i>

                    <span>Chef detail</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>chef_details/add_chef.php"><i class="fa fa-angle-double-right"></i> Add Chef</a></li>
                    <li><a href="<?= APP_PATH ?>chef_details/chef_details_list.php"><i class="fa fa-angle-double-right"></i> Chef List</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cutlery" aria-hidden="true"></i>

                    <span>Chef recipes</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>chef_recipes/add_chef_recipes.php"><i class="fa fa-angle-double-right"></i> Add Chef recipes</a></li>
                    <li><a href="<?= APP_PATH ?>chef_recipes/chef_recipes_list.php"><i class="fa fa-angle-double-right"></i> Chef recipes List</a></li>
                </ul>
            </li>

         

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>customers</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>customer/list_customers.php"><i class="fa fa-angle-double-right"></i> customers List</a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <span>Customers recipes request</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>customer_recipes_request/list_customer_recipes_request.php"><i class="fa fa-angle-double-right"></i>Customers recipes request List</a></li>


                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wheelchair-alt" aria-hidden="true"></i>
                    <span>wishlist</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>wishlist/list_wishlist.php"><i class="fa fa-angle-double-right"></i>wishlist List</a></li>

                </ul>
            </li>
            
            
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>Contact Request</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= APP_PATH ?>/contact_request/contact_request_list.php"><i class="fa fa-angle-double-right"></i>Contact Request List</a></li>

                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>