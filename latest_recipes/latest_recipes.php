
<div class="entries row">



                         <?php
                       $current_recipes = $obj->FlyQuery("SELECT r.*,
                                IFNULL((SUM(rr.rating)/count(rr.id)),0) as rating,
                                IFNULL((SELECT count(w.id) FROM wishlist as w WHERE w.recipes_id=r.id),0) as wish 
                                FROM `recipes` as r
                                LEFT JOIN recipes_rating as rr ON r.id=rr.recipes_id
                                GROUP BY r.id 
                                ORDER BY r.id DESC
                                LIMIT 8");
    foreach ($current_recipes as $row) :
        ?>
        <div class="entry one-fourth">
            <figure>

                <img src="<?= SITE_IMG_PATH ?><?php echo $row->image; ?>" style="height: 300px !important; width:100%;" alt="recipes_image"  />

                <figcaption><a href="recipe_view.php?rec_id=<?= $row->id ?>"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
            </figure>

            <div class="container">
                <h2 style="height: 60px;"><a href="recipe_view.php?rec_id=<?= $row->id ?>"><?= $row->name ?></a></h2> 
                <div class="actions">

                    <div>
                        <div class="comments" title="please Leave a Rating">
                            <span class="rating">
                                <input type="radio" id="star5" name="rating" value="5" />
                                <label data="<?= $row->id ?>" class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4" name="rating" value="4" />
                                <label data="<?= $row->id ?>" class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3" name="rating" value="3" />
                                <label data="<?= $row->id ?>" class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2" name="rating" value="2" />
                                <label data="<?= $row->id ?>" class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1" name="rating" value="1" />
                                <label data="<?= $row->id ?>" class = "full" for="star1" title="Sucks big time - 1 star"></label>

                            </span>
                            <span class="rating_result" id="rating_result_<?= $row->id ?>"><?= number_format($row->rating) ?></span>


                        </div>
                        <div class="likes wishbar" data-id="<?= $row->id ?>" customer-id="<?= $customer_id ?>" title="Add to Wishlist">
                            <i  class="fa fa-heart wishresult"></i> <span id="wish_result_<?= $row->id ?>"><?= number_format($row->wish) ?></span>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    <?php endforeach; ?>





</div>