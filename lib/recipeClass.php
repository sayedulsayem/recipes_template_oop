<?php

/* Author : Munira */

/**
 * Description of recipeClass
 *
 * @author ASUS
 */
class recipeClass {

    //put your code here

    private function dbCon() {

        $db = new PDO('mysql:host=localhost;dbname=cooking_recipes;charset=utf8mb4', 'root', '');
        return $db;
    }

    public function Insert($table, $insert_array) {

        $field = '';
        $total_field = count($insert_array);
        if ($total_field != 0) {
            $index = 0;
            foreach ($insert_array as $col => $value):
                if ($index != 0) {
                    $field .=",";
                }
                $field .=$col . "='" . $value . "'";
                $index++;
            endforeach;


            $db = $this->dbCon();
            $query = "INSERT INTO `$table` SET $field";
            $result = $db->exec($query);
            if ($result) {
                return 1; //if successfully inserted
            } else {
                return 0; //if failed to inserted
            }
        } else {
            return 2;  //if inserted array is empty
        }
    }

    public function Delete($table, $delete_array) {

        $field = '';
        $total_field = count($delete_array);
        if ($total_field != 0) {
            $index = 0;
            foreach ($delete_array as $col => $value):
                if ($index != 0) {
                    $field .=" AND ";
                }
                $field .=$col . "='" . $value . "'";
                $index++;
            endforeach;


            $db = $this->dbCon();
            $query = "DELETE FROM `$table` WHERE $field";

            $result = $db->exec($query);
            if ($result) {
                return 1; //if successfully Deleted
            } else {
                return 0; //if failed to Delete
            }
        } else {
            return 2;  //if delete array is empty
        }
    }

    public function Update($table, $update_array, $update_condition_array) {

        $field = '';
        $condition_field = '';
        $total_field = count($update_array);
        $total_condition_field = count($update_condition_array);
        if ($total_field != 0 && $total_condition_field != 0) {
            $index = 0;
            foreach ($update_array as $col => $value):
                if ($index != 0) {
                    $field .=",";
                }
                $field .="`" . $col . "`='" . $value . "'";
                $index++;
            endforeach;


            $index_condition = 0;
            foreach ($update_condition_array as $col => $value):
                if ($index_condition != 0) {
                    $condition_field .=" AND ";
                }
                $condition_field .="`" . $col . "`='" . $value . "'";
                $index_condition++;
            endforeach;


            $db = $this->dbCon();
            $query = "UPDATE `$table` SET $field WHERE $condition_field";
            //exit();
            $result = $db->exec($query);
            if ($result) {
                return 1; //if successfully inserted
            } else {
                return 0; //if failed to inserted
            }
        } else {
            return 2;  //if inserted array is empty
        }
    }

    public function SelectAll($table, $order_id, $order) {
        $db = $this->dbCon();
        if (!empty($table)) {
            $sqlarray = [];
            $query = "SELECT * FROM `$table` ORDER BY `$order_id` $order";


            $sql = $db->prepare($query);

            if ($sql->execute()) {

                $data_row = $sql->rowCount();
                if (!empty($data_row)) {
                    $data = $sql->fetchAll(PDO::FETCH_OBJ);
                    return $data;
                } else {
                    return $sqlarray;
                }
            } else {
                return $sqlarray;
            }
        }
    }
    
    public function FlyQuery($query) {
        $db = $this->dbCon();
        if (!empty($query)) {
            $sqlarray = [];
            $sql = $db->prepare($query);

            if ($sql->execute()) {

                $data_row = $sql->rowCount();
                if (!empty($data_row)) {
                    $data = $sql->fetchAll(PDO::FETCH_OBJ);
                    return $data;
                } else {
                    return $sqlarray;
                }
            } else {
                return $sqlarray;
            }
        }
    }

    public function TotalRows($table, $condition_array = array()) {
        $db = $this->dbCon();
        if (!empty($table)) {
            $where_condition = '';
            $total_field = count($condition_array);
            if ($total_field != 0) {
                $index = 0;
                foreach ($delete_array as $col => $value):
                    if ($index != 0) {
                        $field .=" AND ";
                    }
                    $field .=$col . "='" . $value . "'";
                    $index++;
                endforeach;

                $where_condition = ' WHERE ' . $field;
            }



            $sqlarray = [];
            $query = "SELECT * FROM `$table` " . $where_condition;
            $sql = $db->prepare($query);

            if ($sql->execute()) {
                $data_row = $sql->rowCount();

                return $data_row;
            } else {
                return $sqlarray;
            }
        }
    }

    public function SitebaseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "recipes_template_oop/";
        //$web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "email_receeive/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }
    
    public function AdminbaseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "recipes_template_oop/cooking_recipes_admin/";
        //$web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "email_receeive/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }
    
    public function ImagebaseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "recipes_template_oop/cooking_recipes_admin/image/";
        //$web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "email_receeive/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

}
