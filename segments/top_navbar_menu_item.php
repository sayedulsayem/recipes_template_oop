<nav class="main-nav" role="navigation" id="menu">
    <ul>
        <li class="current-menu-item"><a href="<?= APP_PATH ?>index.php" title="Home"><span>Home</span></a></li>


        <li><a href="<?= APP_PATH ?>recipes.php" title="Category"><span>Category</span></a>

            <ul>
                <?php
                $sql = $obj->FlyQuery("SELECT * FROM recipes_category");

                foreach ($sql as $category) :
                    ?>


                    <li>
                        <a href="<?= APP_PATH ?>category.php?cid=<?php echo $category->id; ?>" title="Category Name"> <?php echo $category->name; ?></a>
                    </li>

                    <?php
                endforeach;
                ?>
            </ul>


        </li>
        <?php
        $sql =$obj->FlyQuery("SELECT * FROM recipes_category WHERE is_top_nav='1' LIMIT 4");

        foreach ($sql as $category) :
            ?>


            <li>
                <a href="<?= APP_PATH ?>category.php?cid=<?php echo $category->id; ?>" title="Category Name"><span> <?php echo $category->name; ?></span></a>
            </li>

            <?php
        endforeach;
        ?>
        <li><a href="<?= APP_PATH ?>contact.php" title="Contact"><span>Contact</span></a></li>
        <?php
        if (isset($_SESSION['SESS_USER_ID'])) {
            ?>
        <li><a href="<?= APP_PATH ?>logout.php" title="Logout"><span>Logout</span></a></li>
        <?php } ?>
    </ul>
</nav>

<nav class="user-nav" role="navigation">
    <ul>
        <li class="light"><a href="find_recipe.html" title="Search for recipes"><i class="icon icon-themeenergy_search"></i> <span>Search for recipes</span></a></li>
        <?php
        if (isset($_SESSION['SESS_USER_ID'])) {
            ?>
            <li class="medium"><a href="my_profile.php" title="My account"><i class="icon icon-themeenergy_chef-hat"></i> <span><?= $_SESSION['SESS_USER_NAME'] ?></span></a></li>
        <?php } else { ?>
            <li class="medium"><a href="login.php" title="Login"><i class="icon icon-themeenergy_chef-hat"></i> <span> Login</span></a></li>
        <?php } ?>
        <li class="dark"><a href="submit_recipe.php" title="Submit a recipe"><i class="icon icon-themeenergy_fork-spoon"></i> <span>Submit a recipe</span></a></li>
    </ul>
</nav>