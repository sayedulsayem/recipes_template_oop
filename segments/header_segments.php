<?php
if (isset($_SESSION['SESS_USER_ID'])) {
    $customer_id = $_SESSION['SESS_USER_ID'];
} else {
    $customer_id = 0;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="SocialChef - Social Recipe HTML Template" />
        <meta name="description" content="SocialChef - Social Recipe HTML Template">
        <meta name="author" content="themeenergy.com">


        <title>SocialChef</title>



        <link rel="stylesheet" href="css/icons.css" />
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800" rel="stylesheet">

        <link rel="shortcut icon" href="images/favicon.ico" />
        <!--use cdn by slider-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/msg.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><!--use link by slider-->
        <script src="https://use.fontawesome.com/e808bf9397.js"></script>

    </head>