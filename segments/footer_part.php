<footer class="foot" role="contentinfo">
    <div class="wrap clearfix">
        <div class="row">
            <article class="one-half">
                <h5>DEMO:  About SocialChef Community</h5><hr
                <p>Have some text</p>
            </article>
            <article class="one-fourth">
                <h5>Need help?</h5>
                <p>DEMO: Contact us via phone or email</p>
                <p><em>Demo T:</em>  +1 555 555 555<br /><em>Demo E:</em>  <a href="#">DEMO:socialchef@email.com</a></p>
            </article>
            <article class="one-fourth">
                <h5>Follow us</h5>
                <ul class="social">
                    <li><a href="#" title="facebook"><i class="fa fa-fw fa-facebook"></i></a></li>
                    <li><a href="#" title="youtube"><i class="fa  fa-fw fa-youtube"></i></a></li>
                    <li><a href="#" title="rss"><i class="fa  fa-fw fa-rss"></i></a></li>
                    <li><a href="#" title="gplus"><i class="fa fa-fw fa-google-plus"></i></a></li>
                    <li><a href="#" title="linkedin"><i class="fa fa-fw fa-linkedin"></i></a></li>
                    <li><a href="#" title="twitter"><i class="fa fa-fw fa-twitter"></i></a></li>
                    <li><a href="#" title="pinterest"><i class="fa fa-fw fa-pinterest-p"></i></a></li>
                    <li><a href="#" title="vimeo"><i class="fa fa-fw fa-vimeo"></i></a></li>
                </ul>
            </article>

            <div class="bottom">
                <p class="copy">Copyright 2017 SocialChef. All rights reserved</p>

                <nav class="foot-nav">
                    <ul>
                        <li><a href="index.php" title="Home">Home</a></li>
                        <li><a href="recipes.php" title="Recipes">Recipes</a></li>
<!--                        <li><a href="blog.html" title="Blog">Blog</a></li>-->
                        <li><a href="contact.php" title="Contact">Contact</a></li>    
                        <li><a href="find_recipe.html" title="Search for recipes">Search for recipes</a></li>
                        <li><a href="login.php" title="Login">Login</a></li>	<li><a href="register.php" title="Register">Register</a></li>													
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>
<!--//footer-->
<!--slider-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<!--//slider-->
<script src="js/jquery-3.1.0.min.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/scripts.js"></script>
<script src="js/home.js"></script>	
<script src="ajax/msg.js"></script>


</body>
</html>