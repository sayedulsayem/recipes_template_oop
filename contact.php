<!--/* Author : Munira *-->
<?php
//session_start();
include_once 'lib/settings.php';
include_once 'lib/connection.php';
include_once './segments/header_segments.php';
?>

<body>
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.php" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->
        </div>
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--row-->
            <div class="row">
                <!--content-->
                <section class="content center full-width">
                    <div class="modal container">
                        <form method="post" action="ajax/contact_us.php" name="contactform" id="contactform">
                            <h3>Contact us</h3>
                            <div id="message" class="alert alert-danger"></div>
                            <div class="f-row">
                                <input type="text" name="userName" placeholder="Your name" id="name" />
                            </div>
                            <div class="f-row">
                                <input type="email" name="email" placeholder="Your email" id="email" />
                            </div>
                            <!--                            <div class="f-row">
                                                            <input type="number" name="number" placeholder="Your phone number" id="phone" />
                                                        </div>-->
                            <div class="f-row">
                                <textarea placeholder="Your message" name="user_comments" id="comments"></textarea>
                            </div>
                            <div class="f-row bwrap">
                                <input type="submit" value="Send message" />
                            </div>
                        </form>
                    </div>
                </section>
                <!--//content-->
            </div>
            <!--//row-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->


    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <!--//footer end-->



