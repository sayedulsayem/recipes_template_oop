<?php 
$recipes_video  = $obj->FlyQuery("SELECT rv.*,r.name as rec_name FROM recipes_video as rv
LEFT JOIN recipes as r ON rv.recipes_id=r.id LIMIT 4");

    foreach($recipes_video as $row):
?>
<div class="entry one-fourth">
    <figure>
        
        <iframe src="<?=$row->link?>" height="290px" width="100%" frameborder="0" allowfullscreen ></iframe>
    </figure>
    <div class="container">
        <h2><a href="<?= APP_PATH ?>recipe_view.php?rec_id=<?=$row->recipes_id?>"><?=$row->rec_name?></a></h2> 
        <div class="actions">
            <div>
                <div class="date"><i class="fa fa-calendar"></i><a href="#"><?=date('d M Y',strtotime($row->date))?></a></div>
                
            </div>
        </div>
    </div>
</div>
<?php 

endforeach;
 ?>


